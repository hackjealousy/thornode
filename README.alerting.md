The system currently has an alerting example for Slack.  This is pretty
useful so we'll go through step-by-step how to enable it.

First, you'll need a Slack account.

1. Go to https://slack.com
2. "Try For Free" in the upper-right-hand corner.
3. "Create a Slack Workspace"
4. Enter your email.
5. Enter the code they just sent to your email.
6. Enter a name for your Slack workspace (team name or whatever.)
7. Enter something describing what your team is working on (it will be the channel name, e.g., "monitoring".)
8. Send other team members an invite if you want.

Next, you need to create a simple "app."

9. Open another window, go to https://api.slack.com
10. "Start Building"
11. Name your app. (nodemon or something like that)
12. Select your "Development Slack Workspace" to be the Slack workspace you just created in steps 3 -- 8.
13. Select Add features and functionality "Incoming Webhooks"
14. Activate Incoming Webhooks (set to On)
15. "Add New Webhook to Workspace"
16. Select the channel you created in step 7.  Click "Allow"
17. Copy the Webhook URL just created, you'll need this to configure monitoring.

Finally, you need to configure prometheus to alert to Slack.

18. Change dir to your node-launcher directory.
19. Consider doing a "git pull" to make sure you have the latest bits.  This could be dangerous, so you may just want to skip this.
20. Edit prometheus/values.yaml
21. Remove the '# ' from the start of lines 24 through 66.
22. Place the webhook URL from step 17 on line 28.
23. Place the channel name from step 7 on line 42.
24. "make install-metrics"

You should see a few alerts fire off in your Slack channel.
